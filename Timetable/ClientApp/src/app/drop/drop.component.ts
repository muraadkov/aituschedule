﻿import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { DropService } from './drop.service';
import { ActivatedRoute } from '@angular/router';
import { Timetable } from '../timetable/timetable';
import { Group } from '../timetable/group';


@Component({
    selector: 'cdk-drag-drop-connected-sorting-group-example',
    templateUrl: './drop.component.html'
})
export class DropComponent implements OnInit {

    constructor(private dropservice: DropService, activeRoute: ActivatedRoute) {
        this.id = Number.parseInt(activeRoute.snapshot.params["id"]);
    }


    todo: Timetable[];
    lesson: Timetable = new Timetable();
    schedules: Timetable[];
    groups: Group[];
    tgroup: Timetable[];
    id: number; 
    monday = []; monday1 = []; monday2 = []; monday3 = []; monday4 = []; monday5 = []; monday6 = []; monday7 = []; monday8 = []; monday9 = [];
    tuesday = []; tuesday1 = []; tuesday2 = []; tuesday3 = []; tuesday4 = []; tuesday5 = []; tuesday6 = []; tuesday7 = []; tuesday8 = []; tuesday9 = [];
    thursday = []; thursday1 = []; thursday2 = []; thursday3 = []; thursday4 = []; thursday5 = []; thursday6 = []; thursday7 = []; thursday8 = []; thursday9 = [];
    wednesday = []; wednesday1 = []; wednesday2 = []; wednesday3 = []; wednesday4 = []; wednesday5 = []; wednesday6 = []; wednesday7 = []; wednesday8 = []; wednesday9 = [];
    friday = []; friday1 = []; friday2 = []; friday3 = []; friday4 = []; friday5 = []; friday6 = []; friday7 = []; friday8 = []; friday9 = []; 

    ngOnInit() {
        if (this.id) {
            this.getForWork(this.id);
            this.getByGroup(this.id);
        }
        this.getAll();
        this.getAllGroups();
    }

    getAll() {
        this.dropservice.getAll()
            .subscribe((data: Timetable[]) => this.schedules = data);
    }

    getAllGroups() {
        this.dropservice.getAllGroups()
            .subscribe((data: Group[]) => this.groups = data);
    }

    getForWork(id: number) {
        this.dropservice.getForWork(this.id)
            .subscribe((data: Timetable[]) => this.todo = data);
    }

    getByGroup(id: number) {
        this.dropservice.getByGroup(this.id)
            .subscribe((data: Timetable[]) => this.schedules = data);
    }



    drop(event: CdkDragDrop<string[]>) {
        if (event.container.id != 'cdk-drop-list-0' && event.container.data.length > 0) {
            return
        }
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        }
        else {
            transferArrayItem(event.previousContainer.data,
                event.container.data,
                event.previousIndex,
                event.currentIndex);
            console.log(event.container.id);
        }
        let data: any = event.container.data[event.currentIndex];
        if (event.container.id == 'cdk-drop-list-1' || event.container.id == 'cdk-drop-list-6' || event.container.id == 'cdk-drop-list-11'
            || event.container.id == 'cdk-drop-list-16' || event.container.id == 'cdk-drop-list-21' || event.container.id == 'cdk-drop-list-26'
            || event.container.id == 'cdk-drop-list-31' || event.container.id == 'cdk-drop-list-36' || event.container.id == 'cdk-drop-list-41' || event.container.id == 'cdk-drop-list-46') {
            data.dayId = 1;
            data.day = {
                id: 1,
                call: 'Monday'
            }
        } else if (event.container.id == 'cdk-drop-list-2' || event.container.id == 'cdk-drop-list-7' || event.container.id == 'cdk-drop-list-12'
            || event.container.id == 'cdk-drop-list-17' || event.container.id == 'cdk-drop-list-22' || event.container.id == 'cdk-drop-list-27'
            || event.container.id == 'cdk-drop-list-32' || event.container.id == 'cdk-drop-list-37' || event.container.id == 'cdk-drop-list-42'
            || event.container.id == 'cdk-drop-list-47') {
            data.dayId = 2;
            data.day = {
                id: 2,
                call: 'Tuesday'
            }
        } else if (event.container.id == 'cdk-drop-list-3' || event.container.id == 'cdk-drop-list-8' || event.container.id == 'cdk-drop-list-13'
            || event.container.id == 'cdk-drop-list-18' || event.container.id == 'cdk-drop-list-23' || event.container.id == 'cdk-drop-list-28'
            || event.container.id == 'cdk-drop-list-33' || event.container.id == 'cdk-drop-list-38' || event.container.id == 'cdk-drop-list-43'
            || event.container.id == 'cdk-drop-list-48') {
            data.dayId = 3;
            data.day = {
                id: 3,
                call: 'Thursday'
            }
        } else if (event.container.id == 'cdk-drop-list-4' || event.container.id == 'cdk-drop-list-9' || event.container.id == 'cdk-drop-list-14'
            || event.container.id == 'cdk-drop-list-19' || event.container.id == 'cdk-drop-list-24' || event.container.id == 'cdk-drop-list-29'
            || event.container.id == 'cdk-drop-list-34' || event.container.id == 'cdk-drop-list-39' || event.container.id == 'cdk-drop-list-44'
            || event.container.id == 'cdk-drop-list-49') {
            data.dayId = 4;
            data.day = {
                id: 4,
                call: 'Wednesday'
            }
        } else if (event.container.id == 'cdk-drop-list-5' || event.container.id == 'cdk-drop-list-10' || event.container.id == 'cdk-drop-list-15'
            || event.container.id == 'cdk-drop-list-20' || event.container.id == 'cdk-drop-list-25' || event.container.id == 'cdk-drop-list-30'
            || event.container.id == 'cdk-drop-list-35' || event.container.id == 'cdk-drop-list-40' || event.container.id == 'cdk-drop-list-45'
            || event.container.id == 'cdk-drop-list-50') {
            data.dayId = 5;
            data.day = {
                id: 5,
                call: 'Friday'
            }
        }

        if (event.container.id == 'cdk-drop-list-1' || event.container.id == 'cdk-drop-list-2' || event.container.id == 'cdk-drop-list-3' || event.container.id == 'cdk-drop-list-4' || event.container.id == 'cdk-drop-list-5') {
            data.timeId = 1;
            data.time = {
                id: 1,
                begin: '08:30',
                end: '09:10'
            }
        }

        if (event.container.id == 'cdk-drop-list-6' || event.container.id == 'cdk-drop-list-7' || event.container.id == 'cdk-drop-list-8' || event.container.id == 'cdk-drop-list-9' || event.container.id == 'cdk-drop-list-10') {
            data.timeId = 2;
            data.time = {
                id: 2,
                begin: '09:20',
                end: '10:00'
            }
        }

        if (event.container.id == 'cdk-drop-list-11' || event.container.id == 'cdk-drop-list-12' || event.container.id == 'cdk-drop-list-13' || event.container.id == 'cdk-drop-list-14' || event.container.id == 'cdk-drop-list-15') {
            data.timeId = 3;
            data.time = {
                id: 3,
                begin: '10:20',
                end: '11:00'
            }
        }

        if (event.container.id == 'cdk-drop-list-16' || event.container.id == 'cdk-drop-list-17' || event.container.id == 'cdk-drop-list-18' || event.container.id == 'cdk-drop-list-19' || event.container.id == 'cdk-drop-list-20') {
            data.timeId = 4;
            data.time = {
                id: 4,
                begin: '11:10',
                end: '11:50'
            }
        }

        if (event.container.id == 'cdk-drop-list-21' || event.container.id == 'cdk-drop-list-22' || event.container.id == 'cdk-drop-list-23' || event.container.id == 'cdk-drop-list-24' || event.container.id == 'cdk-drop-list-25') {
            data.timeId = 5;
            data.time = {
                id: 5,
                begin: '12:00',
                end: '12:40'
            }
        }

        if (event.container.id == 'cdk-drop-list-26' || event.container.id == 'cdk-drop-list-27' || event.container.id == 'cdk-drop-list-28' || event.container.id == 'cdk-drop-list-29' || event.container.id == 'cdk-drop-list-30') {
            data.timeId = 6;
            data.time = {
                id: 6,
                begin: '13:30',
                end: '14:10'
            }
        }

        if (event.container.id == 'cdk-drop-list-31' || event.container.id == 'cdk-drop-list-32' || event.container.id == 'cdk-drop-list-33' || event.container.id == 'cdk-drop-list-34' || event.container.id == 'cdk-drop-list-35') {
            data.timeId = 7;
            data.time = {
                id: 7,
                begin: '14:20',
                end: '15:00'
            }
        }

        if (event.container.id == 'cdk-drop-list-36' || event.container.id == 'cdk-drop-list-37' || event.container.id == 'cdk-drop-list-38' || event.container.id == 'cdk-drop-list-39' || event.container.id == 'cdk-drop-list-40') {
            data.timeId = 8;
            data.time = {
                id: 8,
                begin: '15:10',
                end: '15:50'
            }
        }

        if (event.container.id == 'cdk-drop-list-41' || event.container.id == 'cdk-drop-list-42' || event.container.id == 'cdk-drop-list-43' || event.container.id == 'cdk-drop-list-44' || event.container.id == 'cdk-drop-list-45') {
            data.timeId = 9;
            data.time = {
                id: 9,
                begin: '16:00',
                end: '16:40'
            }
        }

        if (event.container.id == 'cdk-drop-list-46' || event.container.id == 'cdk-drop-list-47' || event.container.id == 'cdk-drop-list-48' || event.container.id == 'cdk-drop-list-49' || event.container.id == 'cdk-drop-list-50') {
            data.timeId = 10;
            data.time = {
                id: 10,
                begin: '17:00',
                end: '17:40'
            }
        } 

        if (event.container.id == 'cdk-drop-list-0') {
            data.dayId = 0;
            data.day = null;
            data.timeId = 0;
            data.time = null;
        }


            
            for (var i = 0; i < this.schedules.length; i++) {
                if (event.container.id == 'cdk-drop-list-1') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 1
                        && this.schedules[i].time.id == 1) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else  {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-2') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 2
                        && this.schedules[i].time.id == 1) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-3') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 3
                        && this.schedules[i].time.id == 1) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-4') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 4
                        && this.schedules[i].time.id == 1) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-5') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 5
                        && this.schedules[i].time.id == 1) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-6') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 1
                        && this.schedules[i].time.id == 2) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-7') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 2
                        && this.schedules[i].time.id == 2) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-8') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 3
                        && this.schedules[i].time.id == 2) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-9') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 4
                        && this.schedules[i].time.id == 2) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-10') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 5
                        && this.schedules[i].time.id == 2) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-11') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 1
                        && this.schedules[i].time.id == 3) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-12') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 2
                        && this.schedules[i].time.id == 3) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-13') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 3
                        && this.schedules[i].time.id == 3) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-14') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 4
                        && this.schedules[i].time.id == 3) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-15') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 5
                        && this.schedules[i].time.id == 3) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-16') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 1
                        && this.schedules[i].time.id == 4) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-17') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 2
                        && this.schedules[i].time.id == 4) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-18') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 3
                        && this.schedules[i].time.id == 4) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-19') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 4
                        && this.schedules[i].time.id == 4) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-20') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 5
                        && this.schedules[i].time.id == 4) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-21') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 1
                        && this.schedules[i].time.id == 5) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-22') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 2
                        && this.schedules[i].time.id == 5) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-23') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 3
                        && this.schedules[i].time.id == 5) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-24') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 4
                        && this.schedules[i].time.id == 5) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-25') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 5
                        && this.schedules[i].time.id == 5) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-26') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 1
                        && this.schedules[i].time.id == 6) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-27') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 2
                        && this.schedules[i].time.id == 6) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-28') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 3
                        && this.schedules[i].time.id == 6) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-29') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 4
                        && this.schedules[i].time.id == 6) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-30') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 5
                        && this.schedules[i].time.id == 6) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-31') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 1
                        && this.schedules[i].time.id == 7) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-32') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 2
                        && this.schedules[i].time.id == 7) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-33') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 3
                        && this.schedules[i].time.id == 7) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-34') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 4
                        && this.schedules[i].time.id == 7) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-35') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 5
                        && this.schedules[i].time.id == 7) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-36') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 1
                        && this.schedules[i].time.id == 8) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-37') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 2
                        && this.schedules[i].time.id == 8) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-38') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 3
                        && this.schedules[i].time.id == 8) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-39') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 4
                        && this.schedules[i].time.id == 8) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-40') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 5
                        && this.schedules[i].time.id == 8) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-41') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 1
                        && this.schedules[i].time.id == 9) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-42') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 2
                        && this.schedules[i].time.id == 9) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-43') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 3
                        && this.schedules[i].time.id == 9) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-44') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 4
                        && this.schedules[i].time.id == 9) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-45') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 5
                        && this.schedules[i].time.id == 9) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-46') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 1
                        && this.schedules[i].time.id == 10) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-47') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 2
                        && this.schedules[i].time.id == 10) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-48') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 3
                        && this.schedules[i].time.id == 10) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-49') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 4
                        && this.schedules[i].time.id == 10) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }

                else if (event.container.id == 'cdk-drop-list-50') {
                    if (this.schedules[i].teacher.id == data.teacher.id
                        && this.schedules[i].day.id == 5
                        && this.schedules[i].time.id == 10) {
                        transferArrayItem(event.container.data,
                            event.previousContainer.data,
                            event.currentIndex,
                            event.previousIndex);
                        alert('Teacher is not free!');
                        break;
                    }
                    else {
                        this.dropservice.updateLesson(data, data.id)
                            .subscribe((data: Timetable) => this.schedules.push(data));
                    }
                }
            }

    }


}