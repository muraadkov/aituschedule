﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Timetable } from '../timetable/timetable';


@Injectable()
export class DropService {
    private urlles = "/api/timetable";
    private urlgrp = "/api/group";

    constructor(private http: HttpClient) {
    }

    getForWork(id: number) {
        return this.http.get(this.urlles + '/getforwork/' + id);
    }

    updateLesson(lesson: Timetable, id: number) {
        return this.http.put(this.urlles + '/updatelesson/' + id, lesson);
    }

    createLesson(lesson: Timetable) {
        return this.http.put(this.urlles + '/createlesson', lesson);
    }

    getAll() {
        return this.http.get(this.urlles + '/getlessons');
    }

    getAllGroups() {
        return this.http.get(this.urlgrp + '/getgroups');
    }

    getByGroup(id: number) {
        return this.http.get(this.urlles + '/group/' + id);
    }
}