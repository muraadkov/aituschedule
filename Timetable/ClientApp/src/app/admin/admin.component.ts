import { Component } from '@angular/core';

import * as XLSX from 'xlsx';

type AOA = any[][];

@Component({
  selector: 'app-sheet',
  templateUrl: './admin.component.html',
})

export class AdminComponent {
  index: number = 0;
  data: AOA = [[], []];
  wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
  fileName: string = 'SheetJS.xlsx';

  onFileChange(evt: any) {
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.data = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));



      for (var i = 1; i < this.data.length; i++) {
        for (var j = 1; j < this.data.length; j++) {
          if (this.data[i][0] != this.data[0][0] && this.data[i][0] != null) {
              this.index = i;
            }
                this.data[i][0] = this.data[this.index][0];
        }
      }
    };
      reader.readAsBinaryString(target.files[0]);
      
  }


  export(): void {
  }

}
